#include <cstring>
#include "../../FHE.h"
#include <fstream>
#include "myUtils.h"
#include <NTL/ZZX.h>
#include <iostream>
#include <bitset>

int main(int argc, char **argv) {

	cout << "DECRYPTING" << endl << endl;
	// Read secret key from file
	Timer tReadPrivateKey;
	tReadPrivateKey.start();

	fstream secKeyFile("/home/silvia/Documents/seckey.txt", fstream::in);
	unsigned long m, p, r;
	vector<long> gens, ords;
	readContextBase(secKeyFile, m, p, r, gens, ords);
	FHEcontext context(m, p, r, gens, ords);
	secKeyFile >> context;

	FHESecKey secretKey(context);
	const FHEPubKey& publicKey = secretKey;

	secKeyFile >> secretKey;

	tReadPrivateKey.stop();
	std::cout << "Time for reading private key from disk: "
			<< tReadPrivateKey.elapsed_time() << "s" << std::endl;

	// Read matching from file
	Timer tReadMatching;
	tReadMatching.start();
	fstream matchingFile("/home/silvia/Documents/Poly/matchingPoly.txt");

	vector<Ctxt> mat;
	for (size_t i = 0; i < 3; i++) { // number of queries

		Ctxt ctxt(publicKey);
		matchingFile >> ctxt;
		mat.push_back(ctxt);

	}

	tReadMatching.stop();
	std::cout << "Time for reading matching from disk: "
			<< tReadMatching.elapsed_time() << "s" << std::endl;

	// Read result from file
	Timer tReadResult;
	tReadResult.start();
	std::fstream resultFile("/home/silvia/Documents/Poly/resultPoly.txt", fstream::in);
	assert(resultFile.is_open());

	if (!resultFile) {
		cerr << "Unable to open file result.txt" << endl;
		exit(1);   // call system to stop
	}

	//Reading result from result File
	uint count = 0;
	std::vector<long int> plaintext(10);
	for (;;) {
		std::string token;
		if (!(resultFile >> token)) {

			plaintext.push_back(0);
			plaintext.push_back(0);
			plaintext.push_back(0);
			break;
		} else {
			plaintext[count] = std::stoi(token);
			count++;
		}
	}
	int result[3];

	//Saving last result to be able to operate on them
	for (size_t i = 0; i < 3; i++)
		result[i] = plaintext[i];

	tReadResult.stop();
	std::cout << "Time for reading result from disk: "
			<< tReadResult.elapsed_time() << "s" << std::endl << endl;

	//Decrypt new matching and update the result
	ZZX pTemp;
	int partial[3];

	for (size_t i = 0; i < mat.size(); i++) {

		secretKey.Decrypt(pTemp, mat[i]);

		if (IsZero(pTemp)) {
		//trick to update a vector
			result[i] += 1;
			plaintext[i] = result[i];
			partial[i] = 1;

		} else {
			result[i] += 0;
			partial[i] = 0;
		}

	}

	resultFile.close();

	//Re-open the file to overwrite with the new results
	ofstream updatedResultFile("/home/silvia/Documents/Poly/resultPoly.txt", ios::trunc);

	// Output result to file
	for (size_t i = 0; i < 3; i++) {
		updatedResultFile << plaintext[i] << endl;
	}

	//Visual
	cout << "Partial result" << endl;
	cout << "SYN ACK SYN/ACK" << endl;

	//Result from the current matching file
	for (size_t i = 0; i < 3; i++) {
		cout << "[ " << partial[i] << " ]";
	}
	cout << endl;

	//Result from all the matching file (sum)
	cout << "     TOTAL" << endl;
	cout << "SYN ACK SYN/ACK" << endl;

	for (size_t i = 0; i < 3; i++) {

		cout << "[ " << result[i] << " ]";

	}

	cout << endl;

	updatedResultFile.close();

	return 0;
}
