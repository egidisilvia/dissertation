#include <cstring>
#include "../../FHE.h"
#include <fstream>
#include "myUtils.h"
#include <NTL/ZZX.h>
#include <iostream>
#include <bitset>


int main(int argc, char **argv) {

	cout<< "DECRYPT" << endl<<endl;
	// Read secret key from file
	Timer tReadPrivateKey;
	tReadPrivateKey.start();

	fstream secKeyFile("/home/silvia/Documents/seckey.txt", fstream::in);
	unsigned long m, p, r;
	vector<long> gens, ords;
	readContextBase(secKeyFile, m, p, r, gens, ords);
	FHEcontext context(m, p, r, gens, ords);
	secKeyFile >> context;

	FHESecKey secretKey(context);
	const FHEPubKey& publicKey = secretKey;

	secKeyFile >> secretKey;

	tReadPrivateKey.stop();
	std::cout << "Time for reading private key from disk: "
			<< tReadPrivateKey.elapsed_time() << "s" << std::endl;

	// Read matching from file
	Timer tReadMatching;
	tReadMatching.start();
	fstream matchingFile("/home/silvia/Documents/matching.txt");

	int pLineNumber, totalNumberOfQuery;
	matchingFile >> pLineNumber;
	matchingFile >> totalNumberOfQuery;

	vector<vector<Ctxt>> matching;

	for (size_t i = 0; i < pLineNumber; i++) {
		vector<Ctxt> partialMatching;
		for (size_t j = 0; j < totalNumberOfQuery; j++) {
			Ctxt ctxt(publicKey);

			matchingFile >> ctxt;

			partialMatching.push_back(ctxt);
		}
		matching.push_back(partialMatching);
	}

	tReadMatching.stop();
	std::cout << "Time for reading matching from disk: "
			<< tReadMatching.elapsed_time() << "s" << std::endl;

	// Read result from file
	Timer tReadResult;
	tReadResult.start();
	std::fstream resultFile("/home/silvia/Documents/result.txt", fstream::in);
	assert(resultFile.is_open());

	if (!resultFile) {
		cerr << "Unable to open file result.txt" << endl;
		exit(1);   // call system to stop
	}

	//Reading result from result File
	uint count = 0;
	std::vector<long int> plaintext(10);
	for (;;) {
		std::string token;
		if (!(resultFile >> token)) {

			plaintext.push_back(0);
			plaintext.push_back(0);
			plaintext.push_back(0);
			break;
		} else {
			plaintext[count] = std::stoi(token);
			count++;
		}
	}
	int result[3];

	//Saving last result to be able to operate on them
	for (size_t i = 0; i < totalNumberOfQuery; i++)
		result[i] = plaintext[i];

	tReadResult.stop();
	std::cout << "Time for reading result from disk: "
			<< tReadResult.elapsed_time() << "s" << std::endl;

	//Decrypt new matching and update the result
	ZZX pTemp;

	for (size_t i = 0; i < pLineNumber; i++) {
		for (size_t j = 0; j < totalNumberOfQuery; j++) {

			secretKey.Decrypt(pTemp, matching[i][j]);

			if (IsZero(pTemp)) {
				//trick to update a vector
				result[j] += 1;

			} else {
				result[j] += 0;
			}

		}
	}

	for (size_t j = 0; j < 3; j++) {

		plaintext[j] = result[j];
	}
	resultFile.close();

	//Re-open the file to overwrite with the new results
	ofstream updatedResultFile("/home/silvia/Documents/result.txt", ios::trunc);

	// Output result to file
	for (size_t i = 0; i < 3; i++) {
		updatedResultFile << plaintext[i] << endl;
	}

	//Result from all the matching file (sum)
	cout << "     TOTAL" << endl;
	cout << "SYN  ACK SYN/ACK" << endl;

	for (size_t i = 0; i < 3; i++) {

		cout << "[ " << result[i] << " ]";

	}

	cout << endl;

	updatedResultFile.close();
	return 0;
}
