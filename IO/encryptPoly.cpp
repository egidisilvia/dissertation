#include <string>
#include <iostream>
#include <cstring>
#include "../../FHE.h"
#include <fstream>
#include "myUtils.h"
#include <NTL/ZZX.h>
#include <bitset>


using namespace std;

vector<short> hexToBool(string a) {
	short x;
	std::stringstream ss;
	ss << std::hex << a;
	ss >> x;
	std::string binary = std::bitset<8>(x).to_string(); //to binary
	vector<short> res;
	for (size_t i = 0; i < binary.size(); ++i) { // This converts the char into an int and pushes it into vec

		res.push_back(binary[i] - '0'); // The digits will be in the same order as before
	}
	return res;
}

vector<vector<short>> explode(const string& str, const char& ch) {
	string next;
	vector<vector<short>> result;

	// For each character in the string
	for (string::const_iterator it = str.begin(); it != str.end(); it++) {
		// If we've hit the terminal character
		if (*it == ch) {
			// If we have some characters accumulated
			if (!next.empty()) {
				// Add them to the result vector
				result.push_back(hexToBool(next));
				next.clear();
			}
		} else {
			// Accumulate the next character into the sequence
			next += *it;
		}
	}
	if (!next.empty())
		result.push_back(hexToBool(next));
	return result;
}

vector<vector<Ctxt>> encrypt(vector<vector<short>> pLine,
		const FHEPubKey& publicKey) {

	vector<vector<Ctxt>> cLine;
	Ctxt ctx1(publicKey); // Initialize the first ciphertext (ctx1) using publicKey

	for (size_t i = 0; i < pLine.size(); i++) {
		vector<Ctxt> tmp;

		for (size_t j = 0; j < 8; j++) {
			publicKey.Encrypt(ctx1, to_ZZX(pLine[i][j]));

			tmp.push_back(ctx1);
			if (j == 7)
				cLine.push_back(tmp);
		}

	}
	return cLine;
}

int main(int argc, char **argv) {
	cout<< "ENCRYPT" << endl<<endl;
	// Read the public key from disk
	Timer tReadKeys;
	tReadKeys.start();

	// Parameters needed to reconstruct the context
	unsigned long m, p, r;

	vector<long> gens, ords;

	fstream pubKeyFile("/home/silvia/Documents/pubkey.txt", fstream::in);
	assert(pubKeyFile.is_open());

	// Initializes a context object with some parameters from the file
	readContextBase(pubKeyFile, m, p, r, gens, ords);
	FHEcontext context(m, p, r, gens, ords);

	// Reads the context itself
	pubKeyFile >> context;

	FHEPubKey publicKey(context);
	pubKeyFile >> publicKey;

	pubKeyFile.close();

	tReadKeys.stop();
	std::cout << "Time for reading keys from disk: " << tReadKeys.elapsed_time()
			<< "s" << std::endl;

// Read the message to be encrypted
// This is specific to this example and will not work for messages that are, for example, sentences.
// In this case, it should be done a mapping between strings and a vector of ints (or bits), like Bloom filters or other techniques.
	std::fstream messageFile("/home/silvia/Documents/traffic.txt", fstream::in);
	assert(messageFile.is_open());

	// Output ciphertext to file
	std::fstream ciphertextFile("/home/silvia/Documents/ciphertext.txt",
			fstream::out | fstream::trunc);
	assert(ciphertextFile.is_open());

	std::string flow;
	vector<ZZX> plaintext;

	while (std::getline(messageFile, flow)) {
//		cout << "Message to be encrypted in hexadecimal : " << std::endl;
//
//		cout << flow << endl;
		vector<vector<short>> ptxt;

		ptxt = explode(flow, ' ');

		//Create the vector of polynomial

		ZZX L;
		int h = 0;
		for (size_t i = 0; i < ptxt.size(); i++) {

			L.SetLength(ptxt[i].size() * ptxt.size());
			for (size_t j = 0; j < ptxt[i].size(); j++) {

				SetCoeff(L, j + h, ptxt[i][j]);

			}
			h += 8;

		}
		plaintext.push_back(L);

//		std::cout << "Message to be encrypted after the conversion in binary: "
//				<< std::endl << L << std::endl << endl;
	}
	// Encryption
	Timer tEncryption;
	tEncryption.start();

	// Encrypts the plaintext vector
	ciphertextFile << plaintext.size()<< endl;

	for (size_t i = 0; i < plaintext.size(); i++) {
		Ctxt ctxt(publicKey);

		publicKey.Encrypt(ctxt, plaintext[i]);

		ciphertextFile << ctxt << endl;

	}

	tEncryption.stop();
	std::cout << "Time for encryption: " << tEncryption.elapsed_time() << "s"
			<< std::endl << endl;

	messageFile.close();
	ciphertextFile.close();

	return 0;
}
