#include <cstring>
#include "../../FHE.h"
#include <fstream>
#include "myUtils.h"
#include <NTL/ZZX.h>
#include <iostream>

Ctxt nxor(vector<Ctxt> line, vector<Ctxt> query, vector<short> position,
		Ctxt czzx) {
	vector<Ctxt> tmp;

	for (size_t i = 0; i < query.size(); i++) {

		line[position[i]] += query[i]; //se uguale e' 0

		line[position[i]] += czzx; // diventa 1

		tmp.push_back(line[position[i]]);

	}

	for (size_t i = 1; i < query.size(); i++) { // se match e' una moltiplicazione di 1

		tmp[0].multiplyBy(tmp[i]);

	}

	tmp[0] += czzx; // faccio +1 quindi torna 0 solo se e' 1

	return tmp[0]; //0 se match

}

int main(int argc, char **argv) {

	cout << "MATCHING" << endl << endl;

	// Read the public key from disk
	Timer tReadKeys;
	tReadKeys.start();

	// Parameters needed to reconstruct the context
	unsigned long m, p, r;

	vector<long> gens, ords;

	fstream pubKeyFile("/home/silvia/Documents/pubkey.txt", fstream::in);
	assert(pubKeyFile.is_open());

	// Initializes a context object with some parameters from the file
	readContextBase(pubKeyFile, m, p, r, gens, ords);
	FHEcontext context(m, p, r, gens, ords);

	// Reads the context itself
	pubKeyFile >> context;

	FHEPubKey publicKey(context);
	pubKeyFile >> publicKey;

	pubKeyFile.close();

	tReadKeys.stop();
	std::cout << "Time for reading keys from disk: " << tReadKeys.elapsed_time()
			<< "s" << std::endl;

// Read ciphertext from file
	Timer tReadCiphertext;
	tReadCiphertext.start();
	fstream ciphertextFile("/home/silvia/Documents/Poly/ciphertextPoly.txt",
			fstream::in);

	//Output how many Ctxt we will have
	int pLineDumpNumber;
	ciphertextFile >> pLineDumpNumber;

	vector<Ctxt> ciphertext;

	for (size_t i = 0; i < pLineDumpNumber; i++) {
		Ctxt ctxt(publicKey);
		ciphertextFile >> ctxt;
		ciphertext.push_back(ctxt);

	}

	tReadCiphertext.stop();
	std::cout << "Time for reading ciphertext from disk: "
			<< tReadCiphertext.elapsed_time() << "s" << std::endl;

	//Check the queries

	// Read queries from file
	Timer tReadQuery;
	tReadQuery.start();
	fstream queryFile("/home/silvia/Documents/Poly/encQueryPoly.txt", fstream::in);

	vector<vector<Ctxt>> totalQuery;

	short numberOfQueries, lenghtQuery;

	queryFile >> numberOfQueries;

	vector<vector<short>> totalPositionInFile;

	vector<short> totalQueryNumber;

	short position;

	for (size_t i = 0; i < numberOfQueries; i++) {

		vector<Ctxt> query;

		queryFile >> lenghtQuery;

		vector<short> positionInFile;

		totalQueryNumber.push_back(lenghtQuery);

		for (size_t j = 0; j < lenghtQuery; j++) {

			queryFile >> position;
			positionInFile.push_back(position);

			Ctxt ctxt(publicKey);
			queryFile >> ctxt;
			query.push_back(ctxt);
		}
		totalPositionInFile.push_back(positionInFile);
		totalQuery.push_back(query);
	}

	tReadQuery.stop();
	std::cout << "Time for reading queries from disk: "
			<< tReadQuery.elapsed_time() << "s" << std::endl << endl;

// Output matching to file
	std::fstream matchingFile("/home/silvia/Documents/Poly/matchingPoly.txt",
			fstream::out | fstream::trunc);
	assert(matchingFile.is_open());

	ZZX zzx(1);
	Ctxt czzx(publicKey);
	publicKey.Encrypt(czzx, zzx);

	Timer tMatch;
	tMatch.start();

	for (size_t i = 0; i < numberOfQueries; i++) {
		Ctxt tempVector(publicKey);

		tempVector = nxor(ciphertext, totalQuery[i], totalPositionInFile[i],
				czzx);

		matchingFile << tempVector << endl;
	}

	tMatch.stop();
	std::cout << "Time for performing matching: " << tMatch.elapsed_time()
			<< "s" << std::endl << endl;
	matchingFile.close();

	return 0;
}

