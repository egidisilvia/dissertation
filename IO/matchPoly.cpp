#include <cstring>
#include "../../FHE.h"
#include <fstream>
#include "myUtils.h"
#include <NTL/ZZX.h>
#include <iostream>
#include <bitset>


//Ctxt nxor(Ctxt line, Ctxt query) {
//	vector<Ctxt> tmp;
//
//
//		line[i] += query;
//		tmp.push_back(line[i]);
//
//	}
//	for (size_t i = 1; i < line.size(); i++) {
//		tmp[0] += tmp[i];
//
//	}
//
//	return tmp[0];
//
//}

int main(int argc, char **argv) {
	cout << "MATCH" << endl << endl;
	// Read the public key from disk
	Timer tReadKeys;
	tReadKeys.start();

	// Parameters needed to reconstruct the context
	unsigned long m, p, r;

	vector<long> gens, ords;

	fstream pubKeyFile("/home/silvia/Documents/pubkey.txt", fstream::in);
	assert(pubKeyFile.is_open());

	// Initializes a context object with some parameters from the file
	readContextBase(pubKeyFile, m, p, r, gens, ords);
	FHEcontext context(m, p, r, gens, ords);

	// Reads the context itself
	pubKeyFile >> context;

	FHEPubKey publicKey(context);
	pubKeyFile >> publicKey;

	pubKeyFile.close();

	tReadKeys.stop();
	std::cout << "Time for reading public keys from disk: "
			<< tReadKeys.elapsed_time() << "s" << std::endl;

	// Read ciphertext from file
	Timer tReadCiphertext;
	tReadCiphertext.start();
	fstream ciphertextFile("/home/silvia/Documents/ciphertext.txt",
			fstream::in);

	//Output how many Ctxt we will have
	int pLineNumber;
	ciphertextFile >> pLineNumber;

	vector<Ctxt> ciphertext;

	for (size_t i = 0; i < pLineNumber; i++) {
		Ctxt ctxt(publicKey);
		ciphertextFile >> ctxt;
		ciphertext.push_back(ctxt);

	}

	tReadCiphertext.stop();
	std::cout << "Time for reading ciphertext from disk: "
			<< tReadCiphertext.elapsed_time() << "s" << std::endl;

	//Check the queries

	// Read queries from file
	Timer tReadQuery;
	tReadQuery.start();
	fstream encQueryFile("/home/silvia/Documents/encQueryPoly.txt",
			fstream::in);

	int totalNumberOfQuery;

	encQueryFile >> totalNumberOfQuery;

	vector<Ctxt> query;

	for (size_t i = 0; i < 3; i++) {
		Ctxt ctxt(publicKey);
		encQueryFile >> ctxt;
		query.push_back(ctxt);

	}

	tReadQuery.stop();
	std::cout << "Time for reading queries from disk: "
			<< tReadQuery.elapsed_time() << "s" << std::endl << endl;

// Output matching to file
	std::fstream matchingFile("/home/silvia/Documents/matching.txt",
			fstream::out | fstream::trunc);
	assert(matchingFile.is_open());

	matchingFile << pLineNumber << endl;
	matchingFile << totalNumberOfQuery << endl;

	Timer tMatch;
	tMatch.start();
	Ctxt temp(publicKey);

	for (size_t i = 0; i < pLineNumber; i++) {
		for (size_t j = 0; j < totalNumberOfQuery; j++) {

			temp = ciphertext[i];
			temp += query[j];

			matchingFile << temp << endl;
		}
	}

	tMatch.stop();
	std::cout << "Time for performing matching: " << tMatch.elapsed_time()
			<< "s" << std::endl << endl;

	matchingFile.close();

	encQueryFile.close();
	return 0;
}
