#include "../../FHE.h"
#include <fstream>
#include <bitset>
#include "myUtils.h"

using namespace std;

vector<short> hexToBool(string a) {
	short x;
	std::stringstream ss;
	ss << std::hex << a;
	ss >> x;
	std::string binary = std::bitset<8>(x).to_string(); //to binary
	vector<short> res;
	switch (x) {
	case 6:
		res.push_back(23);
		break;
	case 2:
		res.push_back(47);
		break;
	case 16: // hex 10
		res.push_back(47);
		break;
	case 18: // hex 12
		res.push_back(47);
		break;
	case 0:
		res.push_back(0);
		break;
	case 1:
		res.push_back(0);
		break;
	default:
		res.push_back(0);
		break;
	}

	for (size_t i = 0; i < binary.size(); ++i) { // This converts the char into an int and pushes it into vec

		res.push_back(binary[i] - '0'); // The digits will be in the same order as before
	}
	return res;
}

vector<vector<short>> explode(const string& str, const char& ch) {
	string next;
	vector<vector<short>> result;

	// For each character in the string
	for (string::const_iterator it = str.begin(); it != str.end(); it++) {
		// If we've hit the terminal character
		if (*it == ch) {
			// If we have some characters accumulated
			if (!next.empty()) {
				// Add them to the result vector
				result.push_back(hexToBool(next));
				next.clear();
			}
		} else {
			// Accumulate the next character into the sequence
			next += *it;
		}
	}
	if (!next.empty())
		result.push_back(hexToBool(next));
	return result;
}

int main(int argc, char **argv) {
	// Read the public key from disk
	Timer tReadKeys;
	tReadKeys.start();

	// Parameters needed to reconstruct the context
	unsigned long m, p, r;

	vector<long> gens, ords;

	fstream pubKeyFile("/home/silvia/Documents/pubkey.txt", fstream::in);
	assert(pubKeyFile.is_open());

	// Initializes a context object with some parameters from the file
	readContextBase(pubKeyFile, m, p, r, gens, ords);
	FHEcontext context(m, p, r, gens, ords);

	// Reads the context itself
	pubKeyFile >> context;

	FHEPubKey publicKey(context);
	pubKeyFile >> publicKey;

	pubKeyFile.close();

	tReadKeys.stop();
	std::cout << "Time for reading keys from disk: " << tReadKeys.elapsed_time()
			<< "s" << std::endl << endl;

	cout << "MAKING QUERY" << endl << endl;

	//Read query from file
	std::fstream queryFile("/home/silvia/Documents/Poly/queryPoly.txt", fstream::in);
	assert(queryFile.is_open());

	// Output ciphertext to file
	std::fstream encQueryFile("/home/silvia/Documents/Poly/encQueryPoly.txt",
			fstream::out | fstream::trunc);
	assert(encQueryFile.is_open());

	int totalQuery = 3;

	encQueryFile << totalQuery << endl;

	std::string flow1;
	while (std::getline(queryFile, flow1)) {
		vector<vector<short>> qtxt;

		qtxt = explode(flow1, ' ');

		int lenghtQuery = qtxt.size();

		encQueryFile << lenghtQuery << endl;

		for (size_t i = 0; i < lenghtQuery; i++) {

			ZZX Q;
			int a = qtxt[i].size() - 1;

			Q.SetLength(a);

			int position = qtxt[i][0];

			for (size_t j = 0; j < a; j++) {

				SetCoeff(Q, j, qtxt[i][j + 1]);

			}

			encQueryFile << position << endl;

			Ctxt ctxt(publicKey);

			publicKey.Encrypt(ctxt, Q);

			encQueryFile << ctxt << endl;

		}

	}
	encQueryFile.close();
	queryFile.close();
	return 0;
}

