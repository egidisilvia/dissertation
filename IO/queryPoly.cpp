#include "../../FHE.h"
#include <fstream>
#include <bitset>
#include "myUtils.h"

using namespace std;


void hexToBool(string a, vector<short>& query) {
	short x;
	std::stringstream ss;
	ss << std::hex << a;
	ss >> x;
	std::string binary = std::bitset<8>(x).to_string(); //to binary

	for (size_t i = 0; i < binary.size(); ++i) { // This converts the char into an int and pushes it into vec
		query.push_back(binary[i] - '0'); // The digits will be in the same order as before
	}
}

void explode(const string& str, const char& ch, vector<short>& query) {
	string next;
	vector<vector<short>> result;

	// For each character in the string
	for (string::const_iterator it = str.begin(); it != str.end(); it++) {
		// If we've hit the terminal character
		if (*it == ch) {
			// If we have some characters accumulated
			if (!next.empty()) {
				// Add them to the result vector
				hexToBool(next, query);
				next.clear();
			}
		} else {
			// Accumulate the next character into the sequence
			next += *it;
		}
	}
	if (!next.empty())
		hexToBool(next, query);

}

int main(int argc, char **argv) {
	cout << "QUERY" << endl << endl;
	// Read the public key from disk
	Timer tReadKeys;
	tReadKeys.start();

	// Parameters needed to reconstruct the context
	unsigned long m, p, r;

	vector<long> gens, ords;

	fstream pubKeyFile("/home/silvia/Documents/pubkey.txt", fstream::in);
	assert(pubKeyFile.is_open());

	// Initializes a context object with some parameters from the file
	readContextBase(pubKeyFile, m, p, r, gens, ords);
	FHEcontext context(m, p, r, gens, ords);

	// Reads the context itself
	pubKeyFile >> context;

	FHEPubKey publicKey(context);
	pubKeyFile >> publicKey;

	pubKeyFile.close();

	tReadKeys.stop();
	std::cout << "Time for reading keys from disk: " << tReadKeys.elapsed_time()
			<< "s" << std::endl << endl;

	//Read query from file
	std::fstream queryFile("/home/silvia/Documents/queryPoly.txt", fstream::in);
	assert(queryFile.is_open());

	// Output ciphertext to file
	std::fstream encQueryFile("/home/silvia/Documents/encQueryPoly.txt",
			fstream::out | fstream::trunc);
	assert(encQueryFile.is_open());

	int totalQuery;
	string a;
	getline(queryFile, a);
	totalQuery = stoi(a);


	encQueryFile << totalQuery << endl;

	std::string flow;
	while (std::getline(queryFile, flow)) {


		vector<short> q;
		explode(flow, ' ', q);

		ZZX Q;
		int SIZE = q.size();

		Q.SetLength(SIZE);
		for (size_t i = 0; i < SIZE; i++) {

			SetCoeff(Q, i, q[i]);
		}

		Ctxt ctxt(publicKey);

		publicKey.Encrypt(ctxt, Q);

		encQueryFile << ctxt << endl;

	}

	queryFile.close();
	encQueryFile.close();
	return 0;
}
