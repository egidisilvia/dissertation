#include <cstring>
#include "../../FHE.h"
#include <fstream>
#include "myUtils.h"
#include <NTL/ZZX.h>
#include <iostream>
#include <bitset>


int main(int argc, char **argv) {

	// Read result from file

	Timer tReadResult;
	tReadResult.start();
	fstream resultFile;

	resultFile.open("/home/silvia/Documents/result.txt");

	if (!resultFile) {
		cerr << "Unable to open file result.txt" << endl;
		exit(1);   // call system to stop
	}

	std::string flow;
	int result[3];
	int count = 0;
	while (std::getline(resultFile, flow)) {
		int x;
		x = stoi(flow);
		result[count] = x;

		count++;
	}

	cout << "     TOTAL" << endl;
	cout << "SYN ACK SYN/ACK" << endl;
	for (size_t i = 0; i < 3; i++) {

		cout << "[ " << result[i] << " ]";

	}

	cout << endl;
	resultFile.close();
	return 0;
}
