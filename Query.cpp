#include <string>
#include <iostream>
#include "../FHE.h"

using namespace std;

vector<short> hexToBool(string a) {
	short x;
	std::stringstream ss;
	ss << std::hex << a;
	ss >> x;
	std::string binary = std::bitset<8>(x).to_string(); //to binary
	vector<short> res;
	for (size_t i = 0; i < binary.size(); ++i) { // This converts the char into an int and pushes it into vec
		res.push_back(binary[i] - '0'); // The digits will be in the same order as before
	}
	return res;
}

class Query {


public:
	vector<ZZX> query(string a) {

		ZZX zzx0(0);
		ZZX zzx1(1);

		if (!a.compare("TCP")) {

			vector<ZZX> cTcp; //06

			cTcp.push_back(zzx0);
			cTcp.push_back(zzx0);
			cTcp.push_back(zzx0);
			cTcp.push_back(zzx0);

			cTcp.push_back(zzx0);
			cTcp.push_back(zzx1);
			cTcp.push_back(zzx1);
			cTcp.push_back(zzx0);
			return cTcp;

		} else if (!a.compare("SYN")) {

			vector<ZZX> cSyn; //02

			cSyn.push_back(zzx0);
			cSyn.push_back(zzx0);
			cSyn.push_back(zzx0);
			cSyn.push_back(zzx0);

			cSyn.push_back(zzx0);
			cSyn.push_back(zzx0);
			cSyn.push_back(zzx1);
			cSyn.push_back(zzx0);
			return cSyn;

		} else if (!a.compare("SYN/ACK")) {

			vector<ZZX> cSynAck; 	//12

			cSynAck.push_back(zzx0);
			cSynAck.push_back(zzx0);
			cSynAck.push_back(zzx0);
			cSynAck.push_back(zzx1);

			cSynAck.push_back(zzx0);
			cSynAck.push_back(zzx0);
			cSynAck.push_back(zzx1);
			cSynAck.push_back(zzx0);
			return cSynAck;

		} else if (!a.compare("ACK")) {

			vector<ZZX> cAck; 	//10

			cAck.push_back(zzx0);
			cAck.push_back(zzx0);
			cAck.push_back(zzx0);
			cAck.push_back(zzx1);

			cAck.push_back(zzx0);
			cAck.push_back(zzx0);
			cAck.push_back(zzx0);
			cAck.push_back(zzx0);
			return cAck;
		}

	}

};
