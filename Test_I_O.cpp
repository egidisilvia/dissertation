#include <fstream>
#include <unistd.h>

#include <NTL/ZZX.h>
#include <NTL/vector.h>

#include "../FHE.h"
//#include "timing.h"

int main(int argc, char *argv[]) {

	long m = 0;                   // Specific modulus
	long p = 2;       // Plaintext base [default=2], should be a prime number
	long r = 2;                   // Lifting [default=1]
	long L = 16;    // Number of levels in the modulus chain [default=heuristic]
	long c = 2;         // Number of columns in key-switching matrix [default=2]
	long w = 64;                  // Hamming weight of secret key
	long d = 0;                   // Degree of the field extension [default=1]
	long k = 80;                 // Security parameter [default=80]
	long s = 0;

	long ptxtSpace = power_long(p, r);

	m = FindM(k, L, c, p, d, s, 0);
	FHEcontext contexts(m, p, r);

	{

		fstream keyFile("/home/silvia/Documents/iotest.txt",
				fstream::out | fstream::trunc);
		assert(keyFile.is_open());

//		m = FindM(k, L, c, p, d, s, 0);

//		contexts = contexts(m, p, r);
		FHESecKey sKeys(contexts);
		const FHEPubKey& publicKey = sKeys;
		Ctxt ctxts(publicKey);
		ZZX ptxts;

		buildModChain(contexts, L, c);
		ZZX G = contexts.alMod.getFactorsOverZZ()[0];

		// Output the FHEcontext to file
		writeContextBase(keyFile, contexts);
		writeContextBase(cout, contexts);
		keyFile << contexts << endl;

//		sKeys(contexts);
//		publicKey = sKeys;
		sKeys.GenSecKey(w);

		// Output the secret key to file, twice. Below we will have two copies
		// of most things.
		keyFile << sKeys << endl;
		keyFile << sKeys << endl;

		// output the plaintext
		ZZX zzx0(1);

		keyFile << "[ ";
		keyFile << zzx0 << " ";
		keyFile << "]\n";

//		ctxts(publicKey);
		// Output the ciphertext to file
		publicKey.Encrypt(ctxts, zzx0);

		keyFile << ctxts << endl;
		keyFile << ctxts << endl;
		keyFile.close();
		cout << "fino a qui tutto ok " << endl;
	}

	// open file for read
	{
		fstream keyFile("/home/silvia/Documents/iotest.txt", fstream::in);

		// Read context from file
		unsigned long m1, p1, r1;
		vector<long> gens, ords;
		readContextBase(keyFile, m1, p1, r1, gens, ords);
		FHEcontext tmpContext(m1, p1, r1, gens, ords);
		keyFile >> tmpContext;
		assert(contexts == tmpContext);

		// We define some things below wrt *contexts[i], not tmpContext.
		// This is because the various operator== methods check equality of
		// references, not equality of the referenced FHEcontext objects.
		FHEcontext& context = contexts;
		FHESecKey secretKey(context);
		FHESecKey secretKey2(tmpContext);
		const FHEPubKey& publicKey = secretKey;
		const FHEPubKey& publicKey2 = secretKey2;

		keyFile >> secretKey;
		keyFile >> secretKey2;
		assert(secretKey == sKeys);

		// Read the plaintext from file
		ZZX a;

		seekPastChar(keyFile, '['); // defined in NumbTh.cpp

		keyFile >> a;

		seekPastChar(keyFile, ']');

		Ctxt ctxt(publicKey);
		Ctxt ctxt2(publicKey2);

		keyFile >> ctxt;
		keyFile >> ctxt2;

		ZZX zzx0, temp2;

		keyFile >> zzx0;

		sKeys.Decrypt(temp2, ctxts);
		assert(zzx0 == temp2);
		cout << "temp2 : " << temp2 << endl;

		secretKey.Decrypt(temp2, ctxts);
		assert(zzx0 == temp2);
		cout << "temp2 : " << temp2 << endl;

		secretKey.Decrypt(temp2, ctxt);
		assert(zzx0 == temp2);
		cout << "temp2 : " << temp2 << endl;

		secretKey2.Decrypt(temp2, ctxt2);
		assert(zzx0 == temp2);

		cout << "temp2 : " << temp2 << endl;
	}

	return 0;
}
