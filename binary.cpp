#include <cstring>
#include "../FHE.h"
#include <fstream>
#include "myUtils.h"
#include <NTL/ZZX.h>
#include <iostream>
#include <bitset>

using namespace std;

vector<Ctxt> hexToBool(string a, const FHEPubKey& publicKey) {
	short x;
	std::stringstream ss;
	ss << std::hex << a;
	ss >> x;
	std::string binary = std::bitset<8>(x).to_string(); //to binary
	vector<Ctxt> res;
	cout << "binary:" << binary << endl;
	for (size_t i = 0; i < binary.size(); i++) { // This converts the char into an int and pushes it into vec
		Ctxt ctxt(publicKey);
		publicKey.Encrypt(ctxt, to_ZZX(binary[i]));
		res.push_back(ctxt); // The digits will be in the same order as before
	}
	return res;
}

vector<vector<Ctxt>> explode(const string& str, const char& ch,
		const FHEPubKey& publicKey) {
	string next;
	vector<vector<Ctxt>> result;

	// For each character in the string
	for (string::const_iterator it = str.begin(); it != str.end(); it++) {
		// If we've hit the terminal character
		if (*it == ch) {
			// If we have some characters accumulated
			if (!next.empty()) {
				// Add them to the result vector
				result.push_back(hexToBool(next, publicKey));
				next.clear();
			}
		} else {
			// Accumulate the next character into the sequence
			next += *it;
		}
	}
	if (!next.empty())
		result.push_back(hexToBool(next, publicKey));
	return result;
}

int main(int argc, char **argv) {

	// Read secret key from file
	Timer tReadPrivateKey;
	tReadPrivateKey.start();

	fstream secKeyFile("/home/silvia/Documents/seckey.txt", fstream::in);
	unsigned long m, p, r;
	vector<long> gens, ords;
	readContextBase(secKeyFile, m, p, r, gens, ords);
	FHEcontext context(m, p, r, gens, ords);
	secKeyFile >> context;

	FHESecKey secretKey(context);
	const FHEPubKey& publicKey = secretKey;

	secKeyFile >> secretKey;

	tReadPrivateKey.stop();
	std::cout << "Time for reading private key from disk: "
			<< tReadPrivateKey.elapsed_time() << "s" << std::endl;

	cout << "READING QUERY" << endl << endl;

	//Read query from file
	std::fstream queryFile("/home/silvia/Documents/Binary/binaryQuery.txt",
			fstream::in);
	assert(queryFile.is_open());

	std::string flow;
	vector<vector<Ctxt>> cQuery;
	while (std::getline(queryFile, flow)) {

		cQuery = explode(flow, ' ', publicKey);	//reading query from file and encrypt bitwise

	}

	queryFile.close();

	cout << "READING MESSAGE" << endl << endl;
	// Read the message to be encrypted
	// This is specific to this example and will not work for messages that are, for example, sentences.
	// In this case, it should be done a mapping between strings and a vector of ints (or bits), like Bloom filters or other techniques.
	std::fstream messageFile("/home/silvia/Documents/Binary/trafficEntireLineBinary.txt",
			fstream::in);
	assert(messageFile.is_open());

	std::string flow1;
	vector<vector<Ctxt>> cMessage;
	while (std::getline(messageFile, flow1)) {

		cMessage = explode(flow1, ' ', publicKey); //reading message from file and encrypt bitwise

	}

	messageFile.close();

	int position[2];
	position[0] = 23;	//hard-coded position
	position[1] = 47;	//hard-coded position
	Ctxt result(publicKey);
	Ctxt ctxt1(publicKey);
	publicKey.Encrypt(ctxt1, to_ZZX(1));
	ZZX pTemp;

	Ctxt temp(publicKey);
//	temp = ctxt1; //initialization of the result, should work as a flag
	result = ctxt1; //same

	for (size_t i = 0; i < cQuery.size(); i++) {

		temp = ctxt1; //initialization of the result, should work as a flag

		for (size_t j = 0; j < cQuery[i].size(); j++) {

			cMessage[position[i]][j] += cQuery[i][j];

			cMessage[position[i]][j] += ctxt1;

			temp.multiplyBy(cMessage[position[i]][j]);

		}
		result.multiplyBy(temp);

	} //this two for loops set the number of Level of the FHE scheme

	secretKey.Decrypt(pTemp, result);
	cout << "pTemp " << pTemp << endl;
	cout << "Found?" << endl;

	if (IsOne(pTemp)) {

		std::cout << "YES!" << endl;
	} else {
		std::cout << "NO." << endl;
	}
	return 0;
}

