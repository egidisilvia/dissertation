#include "../FHE.h"
#include <bitset>
#include "Query.cpp"
#include <fstream>
#include "IO/myUtils.h"

//#include "IO/keyGenerator.cpp"

//using namespace std;
//
//vector<short> hexToBool(string a) {
//	short x;
//	std::stringstream ss;
//	ss << std::hex << a;
//	ss >> x;
//	std::string binary = std::bitset<8>(x).to_string(); //to binary
//	vector<short> res;
//	for (size_t i = 0; i < binary.size(); ++i) { // This converts the char into an int and pushes it into vec
//
//		res.push_back(binary[i] - '0'); // The digits will be in the same order as before
//	}
//	return res;
//}

Ctxt nxor(vector<Ctxt> line, vector<ZZX> query) {
	vector<Ctxt> tmp;
	for (short i = 0; i < 8; i++) {

		line[i].nxorConstant(query[i], -1.0);
		tmp.push_back(line[i]);

	}
	tmp[0].multiplyBy(tmp[1]);
	tmp[0].multiplyBy(tmp[2]);
	tmp[0].multiplyBy(tmp[3]);
	tmp[0].multiplyBy(tmp[4]);
	tmp[0].multiplyBy(tmp[5]);
	tmp[0].multiplyBy(tmp[6]);
	tmp[0].multiplyBy(tmp[7]);

	return tmp[0];

}

vector<vector<short>> explode(const string& str, const char& ch) {
	string next;
	vector<vector<short>> result;

	// For each character in the string
	for (string::const_iterator it = str.begin(); it != str.end(); it++) {
		// If we've hit the terminal character
		if (*it == ch) {
			// If we have some characters accumulated
			if (!next.empty()) {
				// Add them to the result vector
				result.push_back(hexToBool(next));
				next.clear();
			}
		} else {
			// Accumulate the next character into the sequence
			next += *it;
		}
	}
	if (!next.empty())
		result.push_back(hexToBool(next));
	return result;
}

vector<vector<Ctxt>> encrypt(vector<vector<short>> pLine,
		const FHEPubKey& publicKey) {

	vector<vector<Ctxt>> cLine;
	Ctxt ctx1(publicKey); // Initialize the first ciphertext (ctx1) using publicKey

	for (size_t i = 0; i < pLine.size(); i++) {			//size is 73
		vector<Ctxt> tmp;

		for (size_t j = 0; j < 8; j++) {
			publicKey.Encrypt(ctx1, to_ZZX(pLine[i][j]));

			tmp.push_back(ctx1);
			if (j == 7)
				cLine.push_back(tmp);
		}

	}
	return cLine;
}

int main(int argc, char **argv) {
	Timer tInitialization;
		tInitialization.start();

	/*** BEGIN INITIALIZATION ***/
	long m = 0;                   // Specific modulus
	long p = 2;       // Plaintext base [default=2], should be a prime number
	long r = 2;                   // Lifting [default=1]
	long L = 16;    // Number of levels in the modulus chain [default=heuristic]
	long c = 2;         // Number of columns in key-switching matrix [default=2]
	long w = 64;                  // Hamming weight of secret key
	long d = 0;                   // Degree of the field extension [default=1]
	long k = 80;                 // Security parameter [default=80]
	long s = 0;                   // Minimum number of slots [default=0]

	std::cout << "Finding m... " << std::flush;
	m = FindM(k, L, c, p, d, s, 0); // Find a value for m given the specified values
	std::cout << "m = " << m << std::endl;

	std::cout << "Initializing context... " << std::flush;
	FHEcontext context(m, p, r); 	                       // Initialize context
	buildModChain(context, L, c); // Modify the context, adding primes to the modulus chain
	std::cout << "OK!" << std::endl;

	std::cout << "Creating polynomial... " << std::flush;
	ZZX G = context.alMod.getFactorsOverZZ()[0]; // Creates the polynomial used to encrypt the data
	std::cout << "OK!" << std::endl;

	std::cout << "Generating keys... " << std::flush;
	FHESecKey secretKey(context);            // Construct a secret key structure
	const FHEPubKey& publicKey = secretKey; // An "upcast": FHESecKey is a subclass of FHEPubKey
	secretKey.GenSecKey(w); // Actually generate a secret key with Hamming weight w
	std::cout << "OK!" << std::endl;
	tInitialization.stop();
		std::cout << "Time for initialization: " << tInitialization.elapsed_time()
				<< "s" << std::endl;
	/*** END INITIALIZATION ***/

//	// Read the public key from disk
//	Timer tReadKeys;
//	tReadKeys.start();
//
//	// Parameters needed to reconstruct the context
//	unsigned long m, p, r;
//
//	vector<long> gens, ords;
//
//	fstream pubKeyFile("/home/silvia/Documents/pubkey.txt", fstream::in);
//	assert(pubKeyFile.is_open());
//
//	// Initializes a context object with some parameters from the file
//	readContextBase(pubKeyFile, m, p, r, gens, ords);
//	FHEcontext context(m, p, r, gens, ords);
//
//	// Reads the context itself
//	pubKeyFile >> context;
//
//	FHEPubKey publicKey(context);
//	pubKeyFile >> publicKey;
//
//	pubKeyFile.close();
//
//	tReadKeys.stop();
//	std::cout << "Time for reading keys from disk: " << tReadKeys.elapsed_time()
//			<< "s" << std::endl;

	vector<ZZX> cSix;

	string q1 = "TCP";
	string q2 = "SYN"; //47 position
	string q3 = "SYN/ACK";
	string q4 = "ACK";

	Query qu;

	vector<vector<ZZX>> query;
	query.push_back(qu.query(q1)); //query[0] TCP
	query.push_back(qu.query(q2)); //query[1] SYN
	query.push_back(qu.query(q3)); //query[2] SYN/ACK
	query.push_back(qu.query(q4)); //query[3] ACK

	ifstream inFile;

	inFile.open("/home/silvia/Documents/trafficLineBinary.txt");

	if (!inFile) {
		cerr << "Unable to open file traffic.txt";
		exit(1);   // call system to stop
	}

	std::string flow;

	vector<vector<ZZX>> totalResult;

	vector<vector<Ctxt>> sumResult;

	auto start = chrono::steady_clock::now();

	while (std::getline(inFile, flow)) {

		vector<vector<short>> pLine;

		vector<vector<Ctxt>> cLine;

		vector<Ctxt> partialCResult;

		pLine = explode(flow, ' ');

		cLine = encrypt(pLine, publicKey);

		Ctxt temp(publicKey);		// line first row

		temp = nxor(cLine[1], query[0]);

		temp *= nxor(cLine[3], query[1]);

		partialCResult.push_back(temp);

		Ctxt temp1(publicKey); 	// line second row

		temp1 = nxor(cLine[1], query[0]);

		temp1 *= nxor(cLine[3], query[2]);

		partialCResult.push_back(temp1);

		Ctxt temp2(publicKey);	// line third row

		temp2 = nxor(cLine[1], query[0]);

		temp2 *= nxor(cLine[3], query[3]);

		partialCResult.push_back(temp2);

		sumResult.push_back(partialCResult);

	}
	auto end = chrono::steady_clock::now();
	auto diff = end - start;
	cout << chrono::duration<double, milli>(diff).count() << " ms" << endl;

	ZZX pTemp;

	for (size_t i = 0; i < sumResult.size(); i++) {

		vector<ZZX> tmp;

		for (size_t j = 0; j < sumResult[i].size(); j++) {

			secretKey.Decrypt(pTemp, sumResult[i][j]);

			tmp.push_back(pTemp);

		}
		totalResult.push_back(tmp);

	}

	cout << "Result Matrix" << endl;
	cout << " SYN ACK SYN/ACK" << endl;

	for (size_t i = 0; i < sumResult.size(); i++) {

		std::cout << totalResult[i] << endl;

	}

	return 0;
}
