#include "../FHE.h"
#include <bitset>
#include "Query.cpp"
#include <fstream>

short hexToInt(string a) {
	short x;
	std::stringstream ss;
	ss << std::hex << a;
	ss >> x;
	return x;
}

Ctxt nxor(vector<Ctxt> line, vector<ZZX> query) {
	vector<Ctxt> tmp;
	for (short i = 0; i < 8; i++) {

		line[i].nxorConstant(query[i], -1.0);
		tmp.push_back(line[i]);

	}
	tmp[0].multiplyBy(tmp[1]);
	tmp[0].multiplyBy(tmp[2]);
	tmp[0].multiplyBy(tmp[3]);
	tmp[0].multiplyBy(tmp[4]);
	tmp[0].multiplyBy(tmp[5]);
	tmp[0].multiplyBy(tmp[6]);
	tmp[0].multiplyBy(tmp[7]);

	return tmp[0];

}

vector<vector<short>> explode(const string& str, const char& ch) {
	string next;
	vector<vector<short>> result;

	// For each character in the string
	for (string::const_iterator it = str.begin(); it != str.end(); it++) {
		// If we've hit the terminal character
		if (*it == ch) {
			// If we have some characters accumulated
			if (!next.empty()) {
				// Add them to the result vector
				result.push_back(hexToBool(next));
				next.clear();
			}
		} else {
			// Accumulate the next character into the sequence
			next += *it;
		}
	}
	if (!next.empty())
		result.push_back(hexToBool(next));
	return result;
}

vector<vector<Ctxt>> encrypt(vector<vector<short>> pLine,
		const FHEPubKey& publicKey) {

	vector<vector<Ctxt>> cLine;
	Ctxt ctx1(publicKey); // Initialize the first ciphertext (ctx1) using publicKey

	for (size_t i = 0; i < pLine.size(); i++) {			//size is 73
		vector<Ctxt> tmp;

		for (size_t j = 0; j < 8; j++) {
			publicKey.Encrypt(ctx1, to_ZZX(pLine[i][j]));

			tmp.push_back(ctx1);
			if (j == 7)
				cLine.push_back(tmp);
		}

	}
	return cLine;
}

int main(int argc, char **argv) {

	/*** BEGIN INITIALIZATION ***/
	long m = 0;                   // Specific modulus
	long p = 2;       // Plaintext base [default=2], should be a prime number
	long r = 1;                   // Lifting [default=1]
	long L = 16;    // Number of levels in the modulus chain [default=heuristic]
	long c = 2;         // Number of columns in key-switching matrix [default=2]
	long w = 64;                  // Hamming weight of secret key
	long d = 0;                   // Degree of the field extension [default=1]
	long k = 80;                 // Security parameter [default=80]
	long s = 0;                   // Minimum number of slots [default=0]

	std::cout << "Finding m... " << std::flush;
	m = FindM(k, L, c, p, d, s, 0); // Find a value for m given the specified values
	std::cout << "m = " << m << std::endl;

	std::cout << "Initializing context... " << std::flush;
	FHEcontext context(m, p, r); 	                       // Initialize context
	buildModChain(context, L, c); // Modify the context, adding primes to the modulus chain
	std::cout << "OK!" << std::endl;

	std::cout << "Creating polynomial... " << std::flush;
	ZZX G = context.alMod.getFactorsOverZZ()[0]; // Creates the polynomial used to encrypt the data
	std::cout << "OK!" << std::endl;

	std::cout << "Generating keys... " << std::flush;
	FHESecKey secretKey(context);            // Construct a secret key structure
	const FHEPubKey& publicKey = secretKey; // An "upcast": FHESecKey is a subclass of FHEPubKey
	secretKey.GenSecKey(w); // Actually generate a secret key with Hamming weight w
	std::cout << "OK!" << std::endl;
	/*** END INITIALIZATION ***/

//	std::string flow1 =
//			"00 00 00 00 00 00 00 00 00 00 00 00 08 00 45 00 00 3c d9 bb 40 00 40 06 62 fe 7f 00 00 01 7f 00 00 01 ab 1e 00 50 21 34 44 4f 00 00 00 00 a0 02 aa aa fe 30 00 00 02 04 ff d7 04 02 08 0a 0f 80 9d f4 00 00 00 00 01 03 03 07";
//
//	std::string flow2 =
//			"00 00 00 00 00 00 00 00 00 00 00 00 08 00 45 00 00 3c 00 00 40 00 40 06 3c ba 7f 00 00 01 7f 00 00 01 00 50 ab 1e 89 e9 80 f7 21 34 44 50 a0 12 aa aa fe 30 00 00 02 04 ff d7 04 02 08 0a 0f 80 9d f4 0f 80 9d f4 01 03 03 07";
	std::string flow1 = "40 06 a0 02";
	std::string flow2 = "40 06 a0 12";
//	std::string flow3 = "40 06 a0 12";
//	std::string flow4 = "40 06 a0 12";
//	std::string flow5 = "40 06 80 10";

	vector<vector<short>> pLine = explode(flow1, ' ');
	vector<vector<short>> pLine1 = explode(flow2, ' ');
//	vector<vector<short>> pLine2 = explode(flow3, ' ');
//	vector<vector<short>> pLine3 = explode(flow4, ' ');
//	vector<vector<short>> pLine4 = explode(flow5, ' ');

	vector<vector<Ctxt>> cLine;
	cLine = encrypt(pLine, publicKey);

	cout << "Generated cLine0" << endl;

	vector<vector<Ctxt>> cLine1;
	cLine1 = encrypt(pLine1, publicKey);

	cout << "Generated cLine1" << endl;

//	vector<vector<Ctxt>> cLine2;
//	cLine = encrypt(pLine2, publicKey);
//
//	cout << "Generated cLine2" << endl;
//
//	vector<vector<Ctxt>> cLine3;
//	cLine = encrypt(pLine3, publicKey);
//
//	cout << "Generated cLine3" << endl;
//
//	vector<vector<Ctxt>> cLine4;
//	cLine = encrypt(pLine4, publicKey);
//
//	cout << "Generated cLine4" << endl;
//
	vector<vector<vector<Ctxt>>*> matrix;

	matrix.push_back(&cLine);
	matrix.push_back(&cLine1);
//	matrix.push_back(&cLine2);
//	matrix.push_back(&cLine3);
//	matrix.push_back(&cLine4);

	cout << "Generated matrix " << endl;
	vector<ZZX> cSix;

	string q1 = "TCP";
	string q2 = "SYN"; //47 position
	string q3 = "SYN/ACK";
	string q4 = "ACK";

	Query qu;

	vector<vector<ZZX>> query;
	query.push_back(qu.query(q1)); //query[0] TCP
	query.push_back(qu.query(q2)); //query[1] SYN
	query.push_back(qu.query(q3)); //query[2] SYN/ACK
	query.push_back(qu.query(q4)); //query[3] ACK

	vector<vector<Ctxt>> cResult;

	auto start = chrono::steady_clock::now();

	for (size_t i = 0; i < matrix.size(); i++) {

		Ctxt temp(publicKey);

		vector<vector<Ctxt>> line = (*(matrix)[i]);

		temp = nxor(line[1], query[0]);

		temp *= nxor(line[3], query[1]);

		Ctxt temp1(publicKey);

		temp1 = nxor(line[1], query[0]);

		temp1 *= nxor(line[3], query[2]);

		vector<Ctxt> resultLine;

		resultLine.push_back(temp);

		resultLine.push_back(temp1);

		cResult.push_back(resultLine);

	}

	auto end = chrono::steady_clock::now();
	auto diff = end - start;
	cout << chrono::duration<double, milli>(diff).count() << " ms" << endl;

	ZZX ptProd;
	vector<vector<ZZX>> pResult;

	for (size_t i = 0; i < cResult.size(); i++) {

		vector<ZZX> tmp;

		for (size_t j = 0; j < cResult[i].size(); j++) {

			secretKey.Decrypt(ptProd, cResult[i][j]); // Decrypt the ciphertext ctSum into the plaintext ptSum using secretKey

			tmp.push_back(ptProd);

		}
		pResult.push_back(tmp);
		std::cout << "tmp" << tmp << endl;

	}
	std::cout << pResult << endl;




	return 0;
}
