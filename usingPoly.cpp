#include "../FHE.h"
#include <bitset>
#include "Query.cpp"
#include <fstream>

vector<vector<short>> explode(const string& str, const char& ch) {
	string next;
	vector<vector<short>> result;

	// For each character in the string
	for (string::const_iterator it = str.begin(); it != str.end(); it++) {
		// If we've hit the terminal character
		if (*it == ch) {
			// If we have some characters accumulated
			if (!next.empty()) {
				// Add them to the result vector
				result.push_back(hexToBool(next));
				next.clear();
			}
		} else {
			// Accumulate the next character into the sequence
			next += *it;
		}
	}
	if (!next.empty())
		result.push_back(hexToBool(next));
	return result;
}

int main(int argc, char **argv) {

	/*** BEGIN INITIALIZATION ***/
	long m = 0;                   // Specific modulus
	long p = 2;       // Plaintext base [default=2], should be a prime number
	long r = 2;                   // Lifting [default=1]
	long L = 16;    // Number of levels in the modulus chain [default=heuristic]
	long c = 2;         // Number of columns in key-switching matrix [default=2]
	long w = 64;                  // Hamming weight of secret key
	long d = 0;                   // Degree of the field extension [default=1]
	long k = 80;                 // Security parameter [default=80]
	long s = 0;                   // Minimum number of slots [default=0]

	std::cout << "Finding m... " << std::flush;
	m = FindM(k, L, c, p, d, s, 0); // Find a value for m given the specified values
	std::cout << "m = " << m << std::endl;

	std::cout << "Initializing context... " << std::flush;
	FHEcontext context(m, p, r); 	                       // Initialize context
	buildModChain(context, L, c); // Modify the context, adding primes to the modulus chain
	std::cout << "OK!" << std::endl;

	std::cout << "Creating polynomial... " << std::flush;
	ZZX G = context.alMod.getFactorsOverZZ()[0]; // Creates the polynomial used to encrypt the data
	std::cout << "OK!" << std::endl;

	std::cout << "Generating keys... " << std::flush;
	FHESecKey secretKey(context);            // Construct a secret key structure
	const FHEPubKey& publicKey = secretKey; // An "upcast": FHESecKey is a subclass of FHEPubKey
	secretKey.GenSecKey(w); // Actually generate a secret key with Hamming weight w
	std::cout << "OK!" << std::endl;

	/*** END INITIALIZATION ***/

//	ifstream inFile;
//	ifstream inQuery;
//
//	inFile.open("/home/silvia/Documents/traffic.txt");
//	inQuery.open("/home/silvia/Documents/query.txt");
//
//	if (!inFile) {
//		cerr << "Unable to open file datafile.txt";
//		exit(1);   // call system to stop
//	}
//
//	if (!inQuery) {
//		cerr << "Unable to open file datafile.txt";
//		exit(1);   // call system to stop
//	}
//
//	std::string flow;
//
//	vector<ZZX> query;
//	while (std::getline(inQuery, flow)) {
//		vector<vector<short>> pQuery;
//
//		pQuery = explode(flow, ' ');
//
//		int pQuerySize = pQuery.size(), pQuerySize0 = pQuery[0].size();
//
//		int SIZE = pQuerySize0 * pQuerySize;
//
//		ZZX TCPSYN;
//
//		TCPSYN.SetLength(SIZE);
//
//		int h = 0;
//
//		for (size_t i = 0; i < pQuerySize; i++) {
//
//			for (size_t j = 0; j < pQuery[i].size(); j++) {
//
//				SetCoeff(TCPSYN, j + h, pQuery[i][j]);
//
//			}
//			h += 8;
//		}
//
//		query.push_back(TCPSYN);
//
//	}
////	cout << "TCPSYN " << query[0] << endl;
////	cout << "TCPACK " << query[1] << endl;
////	cout << "TCPSYNACK " << query[2] << endl;
////	cout << "query " << query << endl;
//	Ctxt encTCPSYN(publicKey), encTCPACK(publicKey), encTCPSYNACK(publicKey);
//
//	publicKey.Encrypt(encTCPSYN, query[0]);
//	publicKey.Encrypt(encTCPACK, query[1]);
//	publicKey.Encrypt(encTCPSYNACK, query[2]);
//
//	encTCPSYN.nxorConstant(query[0], -1.0);
//	encTCPACK.nxorConstant(query[1], -1.0);
//	encTCPSYNACK.nxorConstant(query[2], -1.0);
//
//	ZZX resultPoly1;
//	secretKey.Decrypt(resultPoly1, encTCPSYN);
//
//	cout << resultPoly1 << endl;
//
//
//	while (std::getline(inFile, flow)) {
//
//		vector<vector<short>> pLine;
//
//		pLine = explode(flow, ' ');
//
//		int pLineSize = pLine.size(), pLineSize0 = pLine[0].size();
//
//		int SIZE = pLineSize0 * pLineSize;
//
//		ZZX U;
//		U.SetLength(SIZE);
//		int h = 0;
//
//		for (size_t i = 0; i < pLineSize; i++) {
//
//			for (size_t j = 0; j < pLine[i].size(); j++) {
//
//				SetCoeff(U, j + h, pLine[i][j]);
//
//			}
//			h += 8;
//		}
//
//		cout << "U" << U << endl;
//		Ctxt encU(publicKey);
//		publicKey.Encrypt(encU, U);
//
//		encU.nxorConstant(query[0], -1.0);
//		if (encU == encTCPSYN)
//			cout << "make it" << endl;
//
//		ZZX resultPoly1;
//		secretKey.Decrypt(resultPoly1, encU);
//
//		cout << resultPoly1 << endl;
//
//	}
	std::string flow1 = "04 06";
	std::string flow2 = "04 06";
	std::string flow3 = "04 06";

	vector<vector<short>> u, v, z;

	u = explode(flow1, ' ');
	v = explode(flow2, ' ');
	z = explode(flow3, ' ');

	ZZX U, V, Z;
	int us = u.size(), us0 = u[0].size();
	int SIZE = (u[0].size()) * (u.size());
//	cout << "SIZE : " << SIZE << endl;
	U.SetLength(SIZE);
	V.SetLength(SIZE);
	Z.SetLength(SIZE);
	int h = 0;

	for (size_t i = 0; i < us; i++) {

		for (size_t j = 0; j < u[i].size(); j++) {

			SetCoeff(U, j + h, u[i][j]);

			SetCoeff(V, j + h, v[i][j]);

			SetCoeff(Z, j + h, z[i][j]);
		}
		h += 8;
	}


	// Ciphertexts that will hold the polynomials encrypted using public key pk
	Ctxt encU(publicKey);
	Ctxt encV(publicKey);


	// Encrypt the polynomials into the ciphertexts
	publicKey.Encrypt(encU, U);
	publicKey.Encrypt(encV, V);

	encU.nxorConstant(Z, -1.0);
	encV.nxorConstant(Z, -1.0);


	if (encU.equalsTo(encV, false))
		cout << "make it" << endl;

	// Decrypt the multiplied ciphertext into a polynomial using the secret key sk
	ZZX resultPoly;

	secretKey.Decrypt(resultPoly, encU);

	cout << resultPoly << endl;

	ZZX resultPoly1;
	secretKey.Decrypt(resultPoly1, encV);

	cout << resultPoly1 << endl;

	return 0;
}
