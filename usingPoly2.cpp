#include "../FHE.h"
#include <bitset>
#include "Query.cpp"
#include <fstream>

vector<vector<short>> explode(const string& str, const char& ch) {
	string next;
	vector<vector<short>> result;

	// For each character in the string
	for (string::const_iterator it = str.begin(); it != str.end(); it++) {
		// If we've hit the terminal character
		if (*it == ch) {
			// If we have some characters accumulated
			if (!next.empty()) {
				// Add them to the result vector
				result.push_back(hexToBool(next));
				next.clear();
			}
		} else {
			// Accumulate the next character into the sequence
			next += *it;
		}
	}
	if (!next.empty())
		result.push_back(hexToBool(next));
	return result;
}

int main(int argc, char **argv) {

	/*** BEGIN INITIALIZATION ***/
	long m = 0;                   // Specific modulus
	long p = 2;       // Plaintext base [default=2], should be a prime number
	long r = 1;                   // Lifting [default=1]
	long L = 16;    // Number of levels in the modulus chain [default=heuristic]
	long c = 2;         // Number of columns in key-switching matrix [default=2]
	long w = 64;                  // Hamming weight of secret key
	long d = 0;                   // Degree of the field extension [default=1]
	long k = 80;                 // Security parameter [default=80]
	long s = 0;                   // Minimum number of slots [default=0]

	std::cout << "Finding m... " << std::flush;
	m = FindM(k, L, c, p, d, s, 0); // Find a value for m given the specified values
	std::cout << "m = " << m << std::endl;

	std::cout << "Initializing context... " << std::flush;
	FHEcontext context(m, p, r); 	                       // Initialize context
	buildModChain(context, L, c); // Modify the context, adding primes to the modulus chain
	std::cout << "OK!" << std::endl;

	std::cout << "Creating polynomial... " << std::flush;
	ZZX G = context.alMod.getFactorsOverZZ()[0]; // Creates the polynomial used to encrypt the data
	std::cout << "OK!" << std::endl;

	std::cout << "Generating keys... " << std::flush;
	FHESecKey secretKey(context);            // Construct a secret key structure
	const FHEPubKey& publicKey = secretKey; // An "upcast": FHESecKey is a subclass of FHEPubKey
	secretKey.GenSecKey(w); // Actually generate a secret key with Hamming weight w
	std::cout << "OK!" << std::endl;

	/*** END INITIALIZATION ***/

	std::string syn = "40 06 a0 02";
	std::string ack = "40 06 80 10";
	std::string synack = "40 06 a0 12";

	vector<vector<short>> synV, ackV, synackV;

	synV = explode(syn, ' ');
	ackV = explode(ack, ' ');
	synackV = explode(synack, ' ');

	ZZX SYN, ACK, SYNACK;
	int us = synV.size(), us0 = synV[0].size();
	int SIZE = (us0) * (us);

	SYN.SetLength(SIZE);
	ACK.SetLength(SIZE);
	SYNACK.SetLength(SIZE);

	int h = 0;

	for (size_t i = 0; i < us; i++) {

		for (size_t j = 0; j < us0; j++) {

			SetCoeff(SYN, j + h, synV[i][j]);

			SetCoeff(ACK, j + h, ackV[i][j]);

			SetCoeff(SYNACK, j + h, synackV[i][j]);
		}
		h += 8;
	}

	vector<ZZX> query;
	query.push_back(SYN);
	query.push_back(ACK);
	query.push_back(SYNACK);

	ifstream inFile;

	inFile.open("/home/silvia/Documents/traffic.txt");

	if (!inFile) {
		cerr << "Unable to open file traffic.txt";
		exit(1);   // call system to stop
	}

	std::string flow;

	vector<vector<int>> totalResult;

	vector<vector<Ctxt>> ctxtResult;

	while (std::getline(inFile, flow)) {

		vector<vector<short>> line;

		line = explode(flow, ' ');

		//Create the polynomial
		ZZX L;

		int lsize = line.size();
		int l0size = line[0].size();

		int SIZE = lsize * l0size;

		L.SetLength(SIZE);

		int h = 0;

		for (size_t i = 0; i < lsize; i++) {

			for (size_t j = 0; j < l0size; j++) {

				SetCoeff(L, j + h, line[i][j]);

			}
			h += 8;
		}

		// Ciphertexts that will hold the polynomials
		//encrypted using public key
		Ctxt encL(publicKey);

		// Encrypt the polynomials into the ciphertexts
		publicKey.Encrypt(encL, L);

		vector<Ctxt> cTemp;
		//Check the queries
		for (size_t i = 0; i < 3; i++) {
			Ctxt temp(publicKey);

			temp = encL;

			temp.nxorConstant(query[i], -1.0);

			cTemp.push_back(temp);

		}
		ctxtResult.push_back(cTemp);
	}

	ZZX pTemp;
	ZZX zzx0(0);
	int sumResult[3];

	for (size_t i = 0; i < 3; i++)
		sumResult[i] = 0;

	for (size_t i = 0; i < ctxtResult.size(); i++) {
		vector<int> tmp;

		for (size_t j = 0; j < ctxtResult[i].size(); j++) {
			secretKey.Decrypt(pTemp, ctxtResult[i][j]);
			if (IsOne(pTemp)) {
				tmp.push_back(1);
				sumResult[j] += 1;
			} else {
				tmp.push_back(0);
			}

		}

		totalResult.push_back(tmp);
	}

	cout << "Result Matrix" << endl;
	cout << " SYN ACK SYN/ACK" << endl;

	for (size_t i = 0; i < totalResult.size(); i++) {

		std::cout << totalResult[i] << endl;

	}

	cout << " SYN ACK SYN/ACK" << endl;

	for (size_t i = 0; i < 3; i++) {

		cout << "[ " << sumResult[i] << " ]";

	}

	cout << endl;


	return 0;
}
